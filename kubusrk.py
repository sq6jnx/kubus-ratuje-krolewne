import dataclasses
import enum
import math
import random


@enum.unique
class Obszar(enum.Enum):
    LAKA = "łąka"
    LAS = "las"
    MORZE = "morze"
    MUR = "mur"
    PLAZA = "plaża"
    SKALA = "skała"
    STAW = "staw"


@dataclasses.dataclass
class Pozycja:
    x: float = None
    y: float = None

    def __post_init__(self):
        # 999 REM ******** losowanie pozycji *******
        # 1000 LET x=INT(RND(1)*40)+1
        # 1010 LET y=INT(RND(1)*40)+1
        # 1020 RETURN
        if self.x is None or self.y is None:
            self.x, self.y = random.randint(1, 40), random.randint(1, 40)


class KubusRatujeKrolewne:
    def __init__(self):
        #  99 REM ********* pozycje startowe ********
        # 110 LET ok = 0

        # 110 GOSUB 1000
        # 120 GOSUB 2000
        # 130 GOSUB 3000
        # 140 IF ok=0 THEN GOTO 110
        # 150 LET xrycerza=x
        # 160 LET yrycerza=y
        while True:
            self.rycerz = Pozycja()
            obszar = self.jaki_to_obszar(self.rycerz)
            if self.czy_jest_przejscie(obszar):
                break

        # 170 GOSUB 1000
        # 180 GOSUB 2000
        # 190 GOSUB 3000
        # 200 IF ok=0 THEN GOTO 170
        # 210 LET xkrolewny=x
        # 220 LET ykrolewny=y
        while True:
            self.krolewna = Pozycja()
            if self.czy_jest_przejscie(self.jaki_to_obszar(self.krolewna)):
                break

        # 230 GOSUB 1000
        # 240 LET xsmoka=x
        # 250 LET ysmoka=y
        self.smok = Pozycja()

    def mainloop(self):
        # 299 REM ******* komunikat na ekranie *******
        while True:
            # 300 PRINT
            # 310 LET x=xrycerza
            # 320 LET y=yrecerza
            # 330 GOSUB 2000
            # 340 PRINT "Wokół ciebie ": obszar$
            # 350 LET x=xrycerza+1
            # 360 GOSUB 2000
            # 370 PRINT "Po wschodniej stronie "; obszar$
            # 380 LET x=xrycerza-1
            # 390 GOSUB 2000
            # 400 PRINT "Po zachodniej stronie "; obszar$
            # 410 LET x=xrycerza
            # 420 LET y=yrycerza+1
            # 430 GOSUB 2000
            # 440 PRINT "Po południowej stronie "; obszar$
            # 450 LET y=yrycerza-1
            # 460 GOSUB 2000
            # 470 PRINT "Po północnej stronie "; obszar$
            # 480 LET dkrolewny=SQR((xrycerza-xkrolewny)^2+(yrycerza-ykrolewny)^2)
            # 490 LET dsmoka=SQR((xrycerza-xsmoka)^2+(yrycerza-ysmoka)^2)
            # 500 IF dkrolewny<1 THEN PRINT "Znalazłeś królewnę. Jesteście uratowani!": END
            # 510 IF dsmoka<1 THEN PRINT "Pochwycił cię smok. Koniec romantycznej przygody!": END
            # 520 PRINT "Królewna znajduje się w odległości ":INT(10*dkrolewny);" netrów a skoj - "; INT(10*dsmoka_;"metrów."
            print()
            obszar = self.jaki_to_obszar(self.rycerz)
            print("Wokół ciebie " + obszar.value)
            obszar = self.jaki_to_obszar(Pozycja(x=self.rycerz.x + 1, y=self.rycerz.y))
            print("Po wschodniej stronie " + obszar.value)
            obszar = self.jaki_to_obszar(Pozycja(x=self.rycerz.x - 1, y=self.rycerz.y))
            print("Po zachodniej stronie " + obszar.value)
            obszar = self.jaki_to_obszar(Pozycja(x=self.rycerz.x, y=self.rycerz.y + 1))
            print("Po południowej stronie " + obszar.value)
            obszar = self.jaki_to_obszar(Pozycja(x=self.rycerz.x, y=self.rycerz.y - 1))
            print("Po północnej stronie " + obszar.value)
            dkrolewny = self.odleglosc(self.rycerz, self.krolewna)
            dsmoka = self.odleglosc(self.rycerz, self.smok)
            if dkrolewny < 1:
                print("Znalazłeś królewnę. Jesteście uratowani!")
                exit(0)
            if dsmoka < 1:
                print("Pochwycił cię smok. Koniec romantycznej przygody!")
                exit(0)
            print(
                "Królewna znajduje się w odległości "
                + str(int(10 * dkrolewny))
                + " metrów a smok - "
                + str(int(10 * dsmoka))
                + " metrów."
            )

            # 530 PRINT
            print()
            # 540 PRINT "     północ- N"
            print("     północ- N")
            # 550 PRINT "    zachód- W O -wschód"
            print("    zachód- W O -wschód")
            # 560 PRINT "   południe- S"
            print("   południe- S")
            # 570 PRINT
            print()

            while True:
                # 580 PRINT "Dokad idziesz?"
                # 599 REM *********** ruch rycerza **********
                # 600 INPUT odp$
                # 610 x=xrycerza
                # 620 y=yrycerza
                # 630 IF odp$="N" THEN LET y=y-1
                # 640 IF odp$="S" THEN LEY y=y+1
                # 650 IF odp$="W" THEN LET x=x-1
                # 660 IF odp$="O" THEN LET x=x+1
                # 670 GOSUB 2000
                # 680 GOSUB 3000
                # 690 IF ok=0 THEN PRINT "Tam jest "; obszar$: GOTO 580
                print("Dokąd idziesz?")
                odp = input()
                if odp == "N":
                    nowy_rycerz = Pozycja(x=self.rycerz.x, y=self.rycerz.y - 1)
                if odp == "S":
                    nowy_rycerz = Pozycja(x=self.rycerz.x, y=self.rycerz.y + 1)
                if odp == "W":
                    nowy_rycerz = Pozycja(x=self.rycerz.x - 1, y=self.rycerz.y)
                if odp == "O":
                    nowy_rycerz = Pozycja(x=self.rycerz.x + 1, y=self.rycerz.y)
                if not self.czy_jest_przejscie(self.jaki_to_obszar(nowy_rycerz)):
                    print("Tam jest " + obszar.value)
                else:
                    self.rycerz = nowy_rycerz
                    break

            # 700 LET xrycerza=x
            # 710 LET yrycerza=y

            # 799 REM ********** ruch królewny **********
            while True:
                # 800 LET x=xkrolewny
                # 810 LET y=ykrolewny
                # 820 LET krolewna=INT(RND(1)*4+1)
                # 830 IF krolewna=1 THEN LET x=xkrolewny-0.5
                # 840 IF krolewna=2 THEN LET x=xkrolewny+0.5
                # 850 IF krolewna=3 THEN LET y=ykrolewny+0.5
                # 860 IF krolewna=4 THEN LET y=ykrolewny-0.5
                # 870 GOSUB 2000
                # 880 GOSUB 3000
                # 890 IF ok=0 THEN GOTO 800
                ruch_krolewny = random.randint(1, 4)
                if ruch_krolewny == 1:
                    self.krolewna.x -= self.krolewna.x - 0.5
                if ruch_krolewny == 2:
                    self.krolewna.x += 0.5
                if ruch_krolewny == 3:
                    self.krolewna.y -= 0.5
                if ruch_krolewny == 4:
                    self.krolewna.y += 0.5
                if self.czy_jest_przejscie(self.jaki_to_obszar(self.krolewna)):
                    break

            # 899 *********** ruch smoka ************
            # 900 LET xkrolewny=x
            # 910 LET ykrolewny=y

            # 950 LET xsmoka=xsmoka-0.5
            # 960 LET ysmoka=ysmoka-0.5
            # 970 IF xrycerza>xsmoka THEN LET xsmoka=xsmoka+1
            # 990 IF yrycerza>ysmoka THEN LET ysmoka=ysmoka+1
            # 990 GOTO 300
            self.smok = Pozycja(x=self.smok.x - 0.5, y=self.smok.y - 0.5)
            if self.rycerz.x > self.smok.x:
                self.smok = Pozycja(x=self.smok.x + 1, y=self.smok.y)
            if self.rycerz.y > self.smok.y:
                self.smok = Pozycja(x=self.smok.x, y=self.smok.y + 1)

    # 998 REM *********** PODPROGRAMY **********

    @staticmethod
    def jaki_to_obszar(p: Pozycja) -> Obszar:
        # 1999 REM ********* jaki to obszar ********
        # 2000 LET obszar$="las"
        # 2010 IF x<23 THEN LET obszar$="łąka"
        # 2020 IF x=1 OR y=1 OR x=40 OR y=40 THEN LET obaszar$="plaża"
        # 2030 IF y>22 AND y<26 AND x<9 THEN LET obszar$="skała"
        # 2040 IF y>6 AND y<18 AND x>17 AND x<24 THEN LET obszar$="staw"
        # 2050 IF (y=12 AND x>8 AND x<18) OR (y=30 AND x>13 AND x<34) THEN LET obszar$="mur"
        # 2060 IF x<1 OR x>40 OR y<1 OR y>40 THEN LET obszar$="morze"
        # 2070 RETURN
        if p.x < 23:
            return Obszar.LAKA
        if p.x == 1 or p.y == 1 or p.x == 40 or p.y == 40:
            return Obszar.PLAZA
        if p.y > 22 and p.y < 26 and p.x < 9:
            return Obszar.SKALA
        if p.y > 6 and p.y < 18 and p.x > 17 and p.x < 24:
            return Obszar.STAW
        if (p.y == 12 and p.x > 8 and p.x < 18) or (
            p.y == 30 and p.x > 13 and p.x < 34
        ):
            return Obszar.MUR
        if p.x < 1 or p.x > 40 or p.y < 1 or p.y > 40:
            return Obszar.MORZE
        return Obszar.LAS

    @staticmethod
    def czy_jest_przejscie(obszar: Obszar) -> bool:
        # 2999 REM ******* czy jest przejście ********
        # 3000 LET ok=0
        # 3010 IF obszar$="łąka" OR obszar$="las" OR obszar$="plaża" THEN LET ok=1
        # 3020 RETURN
        return obszar in (Obszar.LAKA, Obszar.LAS, Obszar.PLAZA)

    @staticmethod
    def odleglosc(p1: Pozycja, p2: Pozycja) -> float:
        """Porównaj linie 480 i 490 programu w BASIC"""
        return math.sqrt((p1.x - p2.x) ** 2 + (p1.y - p2.y) ** 2)


KubusRatujeKrolewne().mainloop()
